package com.rubythree.joker.model;

import com.rubythree.joker.R;

/**
 * Created by rubythree on 10/6/15.
 */
public class Story {
    private Page[] mPages;
    private int mCurrentPage;

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(int currentPage) {
        mCurrentPage = currentPage;
    }

    public Story() {
        this.mPages = new Page[5];
        mCurrentPage = 0;

        mPages[0] = new Page(R.drawable.joker2," How does Batman take his coffee? \n Black, he is the Prince of Darkness.");
        mPages[1] = new Page(R.drawable.joker3," What is Batman's favorite part of a joke?\n The 'Punch' line.");
        mPages[2] = new Page(R.drawable.joker4," What do you call it when Batman skips church? \n Christian Bale.");
        mPages[3] = new Page(R.drawable.joker5," What position did Bruce Wayne play on his little-league team? \n He was the bat-boy.");
        mPages[4] = new Page(R.drawable.batman, "There is no \n longer hope.\n Finito! ");

    }

    public Page getPage(int pageNumber) {
        return  mPages[pageNumber];
    }
}
