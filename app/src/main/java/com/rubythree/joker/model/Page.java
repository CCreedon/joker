package com.rubythree.joker.model;

/**
 * Created by rubythree on 10/6/15.
 */
public class Page {
    private int mImageId;
    private String mText;


    public Page(int imageId, String text){
        mImageId = imageId;
        mText = text;

    }

    public int getImageId(){

        return mImageId;
    }

    public void setmImageId(int id){
        mImageId = id;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }
}
