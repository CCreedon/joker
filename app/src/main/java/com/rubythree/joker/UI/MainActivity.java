package com.rubythree.joker.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.rubythree.joker.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.mStartButton)
        Button mStartButton;


    @OnClick(R.id.mStartButton)
        public void startStory() {
            Intent intent = new Intent(this, StoryActivity.class);
        Toast.makeText(MainActivity.this, "Let's Play", Toast.LENGTH_SHORT).show();
        startActivity(intent);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }











}
