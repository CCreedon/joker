package com.rubythree.joker.UI;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v4.content.ContextCompat;
import com.rubythree.joker.R;
import com.rubythree.joker.model.Page;
import com.rubythree.joker.model.Story;

import butterknife.Bind;
import butterknife.BindDrawable;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StoryActivity extends AppCompatActivity {

    private Story mStory = new Story();
    @Bind(R.id.storyImageView)
        ImageView mImageView;
    @Bind(R.id.storyTextView)
        TextView mTextView;
    @OnClick(R.id.nextPage)
        void function ()
        {
            nextPage();
        }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);
        ButterKnife.bind(this);
        nextPage();

    }

    private void  nextPage ()
     {  if (mStory.getCurrentPage() == 5)
         return;
        switch(mStory.getCurrentPage())
        {
            case 0:
                mImageView.setImageDrawable(ContextCompat.getDrawable(this.getApplicationContext(), R.drawable.joker2));
                break;
            case 1:
                mImageView.setImageDrawable(ContextCompat.getDrawable(this.getApplicationContext(), R.drawable.joker3));
                break;
            case 2:
                mImageView.setImageDrawable(ContextCompat.getDrawable(this.getApplicationContext(), R.drawable.joker4));
                break;
            case 3:
                mImageView.setImageDrawable(ContextCompat.getDrawable(this.getApplicationContext(), R.drawable.joker5));
                break;
            case 4:
                mImageView.setImageDrawable(ContextCompat.getDrawable(this.getApplicationContext(), R.drawable.batman));
                break;
        }
        mTextView.setText(mStory.getPage(mStory.getCurrentPage()).getText());
        mStory.setCurrentPage(mStory.getCurrentPage()+1);
    }
}

